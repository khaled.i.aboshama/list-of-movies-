package com.khaledismail.popularpeoplemovies.model.network;

import android.content.Context;

import com.khaledismail.popularpeoplemovies.model.network.api.PopularPeopleApi;
import com.khaledismail.popularpeoplemovies.utils.Constants;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkManager {

    public static final int HTTP_CACHE = 10 * 1024 * 1024; // 10 MiB
    private static final long REQUEST_TIMEOUT_MILLISEC = 10 * 1000;
    private static final String HTTP = "http";
    private static NetworkManager instance;
    private Retrofit retrofit;

    private NetworkManager(Context context) {
        provideRetrofit(provideGsonConverterFactory(),
                provideRxJava2CallAdapterFactory(),
                getOkHttpClient(context));
    }


    public static NetworkManager getInstance(Context context) {
        if (instance == null)
            instance = new NetworkManager(context);
        return instance;
    }

    Retrofit provideRetrofit(GsonConverterFactory gsonConverterFactory,
                             RxJava2CallAdapterFactory rxJava2CallAdapterFactory,
                             OkHttpClient okHttpClient) {
        return retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .addConverterFactory(gsonConverterFactory)
                .client(okHttpClient)
                .baseUrl(Constants.API_BASE_URL)
                .build();
    }

    RxJava2CallAdapterFactory provideRxJava2CallAdapterFactory() {
        return RxJava2CallAdapterFactory.create();
    }

    GsonConverterFactory provideGsonConverterFactory() {
        return GsonConverterFactory.create();
    }


    protected OkHttpClient getOkHttpClient(Context context) {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        setLoggingInterceptor(clientBuilder);
        setHeadersInterceptor(clientBuilder);
        clientBuilder.cache(getHttpCache(context));

        setTimeouts(clientBuilder);

        return clientBuilder.build();
    }

    protected void setTimeouts(OkHttpClient.Builder clientBuilder) {
        clientBuilder.connectTimeout(REQUEST_TIMEOUT_MILLISEC, TimeUnit.MILLISECONDS);
        clientBuilder.readTimeout(REQUEST_TIMEOUT_MILLISEC, TimeUnit.MILLISECONDS);
    }


    protected void setLoggingInterceptor(OkHttpClient.Builder clientBuilder) {
        if (isDebug()) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            clientBuilder.addInterceptor(logging);
        }
    }

    private boolean isDebug() {
        return true;
    }

    protected void setHeadersInterceptor(OkHttpClient.Builder clientBuilder) {
        clientBuilder.addInterceptor(chain -> {
            Request original = chain.request();
            Request.Builder requestBuilder = original.newBuilder();
            HttpUrl.Builder httpUrl = original.url().newBuilder();

            Request request = requestBuilder.url(httpUrl.build())
                    .method(original.method(), original.body())
                    .build();

            return chain.proceed(request);
        });
    }

    protected Cache getHttpCache(Context context) {
        final File httpCacheDir = new File(context.getCacheDir(), HTTP);
        final long httpCacheSize = HTTP_CACHE;
        return new Cache(httpCacheDir, httpCacheSize);
    }

    public PopularPeopleApi providePopularPeopleApi() {
        return retrofit.create(PopularPeopleApi.class);
    }
}
