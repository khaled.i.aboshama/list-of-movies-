package com.khaledismail.popularpeoplemovies.model.database;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.khaledismail.popularpeoplemovies.model.database.data.PopularPerson;

import java.util.List;

/**
 * Data Access Object for the table.
 */
@Dao
public interface PopularPeopleDao {

    @Query("SELECT * FROM people WHERE LOWER(name) LIKE LOWER(:query) ORDER BY name ASC")
    LiveData<List<PopularPerson>> getPopularPeople(String query);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertPopularPerson(PopularPerson popularPerson);
}
