package com.khaledismail.popularpeoplemovies.model.network.api;

import com.khaledismail.popularpeoplemovies.model.response.PersonDetailsResponse;
import com.khaledismail.popularpeoplemovies.model.response.PopularPeopleResponse;
import com.khaledismail.popularpeoplemovies.model.response.PopularPersonImagesResponse;
import com.khaledismail.popularpeoplemovies.utils.Constants;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface PopularPeopleApi {
    String PERSON_POPULAR = "person/popular";
    String SEARCH_PERSON = "search/person";
    String PERSON_IMAGES = "person/{person_id}/images";
    String PERSON_DETAILS = "person/{person_id}";

    @GET(PERSON_POPULAR)
    Observable<PopularPeopleResponse> getPopularPeople(
            @Query(Constants.PAGE_REQUEST_PARAM) int page,
            @Query("api_key") String apiKey);

    @GET(SEARCH_PERSON)
    Observable<PopularPeopleResponse> searchPopularPeople(
            @Query(Constants.PAGE_REQUEST_PARAM) int page,
            @Query(Constants.QUERY_REQUEST_PARAM) String query,
            @Query("api_key") String apiKey);

    @GET(PERSON_IMAGES)
    Single<PopularPersonImagesResponse> getPopularPersonImages(
            @Path("person_id") int personId,
            @Query("api_key") String apiKey);

    @GET(PERSON_DETAILS)
    Single<PersonDetailsResponse> getPersonDetails(
            @Path("person_id") int personId,
            @Query("api_key") String apiKey);
}
