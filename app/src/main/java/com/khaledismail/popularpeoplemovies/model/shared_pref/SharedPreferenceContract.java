package com.khaledismail.popularpeoplemovies.model.shared_pref;

public interface SharedPreferenceContract {
    void saveNumber(String key, int pageNumber);

    int getInt(String key);
}
