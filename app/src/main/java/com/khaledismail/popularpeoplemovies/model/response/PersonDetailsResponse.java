package com.khaledismail.popularpeoplemovies.model.response;

import com.google.gson.annotations.SerializedName;
import com.khaledismail.popularpeoplemovies.model.database.data.PopularPerson;

public class PersonDetailsResponse extends PopularPerson {

    private String birthday;
    private String biography;
    @SerializedName("place_of_birth")
    private String placeOfBirth;

    public PersonDetailsResponse(int id, String name, String profilePath, String department) {
        super(id, name, profilePath, department);
        biography = "";
        birthday = "";
        placeOfBirth = "";
    }

    public String getBirthday() {
        return birthday;
    }

    public String getBiography() {
        return biography;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    @Override
    public String getProfilePath() {
        return super.getProfilePath();
    }

}
