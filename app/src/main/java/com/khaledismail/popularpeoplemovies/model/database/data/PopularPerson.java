package com.khaledismail.popularpeoplemovies.model.database.data;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "people", indices = @Index(value = "name"))
public class PopularPerson implements Parcelable {

    public static final Creator<PopularPerson> CREATOR = new Creator<PopularPerson>() {
        @Override
        public PopularPerson createFromParcel(Parcel in) {
            return new PopularPerson(in);
        }

        @Override
        public PopularPerson[] newArray(int size) {
            return new PopularPerson[size];
        }
    };
    public static DiffUtil.ItemCallback<PopularPerson> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<PopularPerson>() {
                @Override
                public boolean areItemsTheSame(@NonNull PopularPerson oldItem,
                                               @NonNull PopularPerson newItem) {
                    return oldItem.getId() == newItem.getId();
                }

                @Override
                public boolean areContentsTheSame(@NonNull PopularPerson oldItem,
                                                  @NonNull PopularPerson newItem) {
                    return oldItem.getId() == newItem.getId();
                }
            };
    @PrimaryKey
    @NonNull
    private int id;
    private String name;
    @SerializedName("profile_path")
    private String profilePath;
    @SerializedName("known_for_department")
    private String department;

    public PopularPerson(int id, String name, String profilePath, String department) {
        this.id = id;
        this.name = name;
        this.profilePath = profilePath;
        this.department = department;
    }

    protected PopularPerson(Parcel in) {
        id = in.readInt();
        name = in.readString();
        profilePath = in.readString();
        department = in.readString();
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getProfilePath() {
        return profilePath;
    }

    public String getDepartment() {
        return department;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeString(profilePath);
        parcel.writeString(department);
    }
}
