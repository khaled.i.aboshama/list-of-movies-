package com.khaledismail.popularpeoplemovies.model.response;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;

import com.google.gson.annotations.SerializedName;

public class Profile {
    public static DiffUtil.ItemCallback<Profile> DIFF_CALLBACK =
            new DiffUtil.ItemCallback<Profile>() {
                @Override
                public boolean areItemsTheSame(@NonNull Profile oldItem,
                                               @NonNull Profile newItem) {
                    return oldItem.profilePath.equalsIgnoreCase(newItem.profilePath);
                }

                @Override
                public boolean areContentsTheSame(@NonNull Profile oldItem,
                                                  @NonNull Profile newItem) {
                    return oldItem.profilePath.equalsIgnoreCase(newItem.profilePath);
                }
            };
    @SerializedName("file_path")
    private String profilePath;

    public Profile(String profilePath) {
        this.profilePath = profilePath;
    }

    public String getProfilePath() {
        return profilePath;
    }
}
