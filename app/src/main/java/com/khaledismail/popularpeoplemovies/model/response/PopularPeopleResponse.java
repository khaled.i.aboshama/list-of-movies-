package com.khaledismail.popularpeoplemovies.model.response;

import com.google.gson.annotations.SerializedName;
import com.khaledismail.popularpeoplemovies.model.database.data.PopularPerson;

import java.util.ArrayList;

public class PopularPeopleResponse {
    @SerializedName("total_pages")
    private int totalPages;
    @SerializedName("page")
    private int pageNumber;
    @SerializedName("results")
    private ArrayList<PopularPerson> popularPersonArrayList;

    public int getTotalPages() {
        return totalPages;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public ArrayList<PopularPerson> getPopularPersonArrayList() {
        return popularPersonArrayList;
    }
}
