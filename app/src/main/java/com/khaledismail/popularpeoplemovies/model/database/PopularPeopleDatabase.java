package com.khaledismail.popularpeoplemovies.model.database;

import android.content.Context;

import androidx.lifecycle.LiveData;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.khaledismail.popularpeoplemovies.model.database.data.PopularPerson;
import com.khaledismail.popularpeoplemovies.utils.Constants;

import java.util.List;

@Database(entities = {PopularPerson.class}, version = 1)
public abstract class PopularPeopleDatabase extends RoomDatabase
        implements PopularPeopleDatabaseContract {

    private static final Object sLock = new Object();
    private static PopularPeopleDatabase instance;

    public static PopularPeopleDatabase getInstance(Context context) {
        synchronized (sLock) {
            if (instance == null) {
                instance = Room.databaseBuilder(context.getApplicationContext(),
                        PopularPeopleDatabase.class, Constants.DATABASE_NAME)
                        .build();
            }
            return instance;
        }
    }

    @Override
    public LiveData<List<PopularPerson>> getPopularPeople(String query) {
        return getPopularPeopleDao().getPopularPeople(query);
    }
}
