package com.khaledismail.popularpeoplemovies.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PopularPersonImagesResponse {
    @SerializedName("profiles")
    private List<Profile> profileArrayList;


    public List<Profile> getProfileArrayList() {
        return profileArrayList;
    }
}
