package com.khaledismail.popularpeoplemovies.model.shared_pref;

import android.content.Context;
import android.content.SharedPreferences;

import com.khaledismail.popularpeoplemovies.utils.Constants;

public class PopularPeopleSharedPreference implements SharedPreferenceContract {

    private static PopularPeopleSharedPreference instance;
    private SharedPreferences sharedPreference;

    private PopularPeopleSharedPreference(Context context) {
        sharedPreference = context.getSharedPreferences(Constants.POPULAR_PEOPLE_SHARED_PREFERENCE,
                Context.MODE_PRIVATE);
    }

    public static PopularPeopleSharedPreference getInstance(Context context) {
        if (instance == null)
            instance = new PopularPeopleSharedPreference(context);
        return instance;
    }

    @Override
    public void saveNumber(String key, int pageNumber) {
        sharedPreference.edit().putInt(key, pageNumber).apply();
    }

    @Override
    public int getInt(String key) {
        return sharedPreference.getInt(key, Constants.ZERO_NUMBER);
    }
}
