package com.khaledismail.popularpeoplemovies.model.database;

import androidx.lifecycle.LiveData;

import com.khaledismail.popularpeoplemovies.model.database.data.PopularPerson;

import java.util.List;

public interface PopularPeopleDatabaseContract {

    LiveData<List<PopularPerson>> getPopularPeople(String query);

    PopularPeopleDao getPopularPeopleDao();
}
