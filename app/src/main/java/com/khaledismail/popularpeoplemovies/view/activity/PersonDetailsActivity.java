package com.khaledismail.popularpeoplemovies.view.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.khaledismail.popularpeoplemovies.R;
import com.khaledismail.popularpeoplemovies.databinding.ActivityPersonDetailsBinding;
import com.khaledismail.popularpeoplemovies.listeners.ProfileImageClickListener;
import com.khaledismail.popularpeoplemovies.model.database.data.PopularPerson;
import com.khaledismail.popularpeoplemovies.model.response.PersonDetailsResponse;
import com.khaledismail.popularpeoplemovies.model.response.Profile;
import com.khaledismail.popularpeoplemovies.utils.Constants;
import com.khaledismail.popularpeoplemovies.utils.Utils;
import com.khaledismail.popularpeoplemovies.view.adapter.PersonImagesAdapter;
import com.khaledismail.popularpeoplemovies.view.custom.GridSpacingItemDecoration;
import com.khaledismail.popularpeoplemovies.viewmodel.PersonDetailsViewModel;
import com.khaledismail.popularpeoplemovies.viewmodel.PersonDetailsViewModelFactory;

public class PersonDetailsActivity extends AppCompatActivity implements ProfileImageClickListener {

    private final static int SPAN_COUNT = 3;
    private final static int GRID_SPACING_ITEM_DECORATION = 6;
    private ActivityPersonDetailsBinding binding;
    private PersonDetailsViewModel personDetailsViewModel;
    private RecyclerView personImagesRecyclerView;
    private PersonDetailsResponse personDetailsResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_person_details);
        setupToolbar();

        parseExtraIntent();
        personDetailsViewModel =
                ViewModelProviders.of(
                        this, new PersonDetailsViewModelFactory(
                                this.getApplication(), personDetailsResponse.getId()))
                        .get(PersonDetailsViewModel.class);
        personDetailsViewModel.setupDataScreen();
        initRecyclerView();
        setObserversOfViewModel();
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitle(getString(R.string.person_details));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    private void setObserversOfViewModel() {
        PersonImagesAdapter personImagesAdapter = new PersonImagesAdapter(this);
        personImagesRecyclerView.setAdapter(personImagesAdapter);

        personDetailsViewModel.getProfileLiveData().observe(
                this, personImagesAdapter::submitList);
        personDetailsViewModel.getPersonDetailsResponseLiveData().observe(
                this, personDetails -> binding.setPersonDetails(personDetails));
    }

    private void parseExtraIntent() {
        PopularPerson popularPerson = getIntent().getParcelableExtra(Constants.Intent.PERSON_KEY);
        if (popularPerson == null)
            return;
        personDetailsResponse = new PersonDetailsResponse(
                popularPerson.getId(), popularPerson.getName(), popularPerson.getProfilePath(),
                popularPerson.getDepartment());
        binding.setPersonDetails(personDetailsResponse);
    }

    private void initRecyclerView() {
        personImagesRecyclerView = binding.recyclerViewPersonImages;
        personImagesRecyclerView.setLayoutManager(new GridLayoutManager(this, SPAN_COUNT));
        personImagesRecyclerView.addItemDecoration(
                new GridSpacingItemDecoration(SPAN_COUNT, Utils.dpToPx(
                        GRID_SPACING_ITEM_DECORATION, this), true));
        personImagesRecyclerView.setItemAnimator(new DefaultItemAnimator());
        personImagesRecyclerView.setNestedScrollingEnabled(false);
    }

    @Override
    public void OnItemClick(Profile profile) {
        openImagePreviewScreen(profile.getProfilePath());
    }

    private void openImagePreviewScreen(String profilePath) {
        Intent intent = new Intent(this, ImagePreviewActivity.class);
        intent.putExtra(Constants.Intent.PROFILE_IMAGE_PATH_KEY, profilePath);
        intent.putExtra(Constants.Intent.PERSON_NAME_KEY, personDetailsResponse.getName());
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        personDetailsViewModel.onDestroy();
        super.onDestroy();
    }
}
