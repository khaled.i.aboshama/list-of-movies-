package com.khaledismail.popularpeoplemovies.view.viewholder;

import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import com.khaledismail.popularpeoplemovies.databinding.ListItemImageProfileBinding;
import com.khaledismail.popularpeoplemovies.listeners.ProfileImageClickListener;

public class ImageViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private ListItemImageProfileBinding binding;
    private ProfileImageClickListener profileImageClickListener;

    public ImageViewHolder(ListItemImageProfileBinding listItemImageProfileBinding,
                           ProfileImageClickListener profileImageClickListener) {
        super(listItemImageProfileBinding.getRoot());
        this.binding = listItemImageProfileBinding;
        binding.getRoot().setOnClickListener(this);
        this.profileImageClickListener = profileImageClickListener;
    }

    public ListItemImageProfileBinding getBinding() {
        return binding;
    }

    @Override
    public void onClick(View view) {
        if (profileImageClickListener != null)
            profileImageClickListener.OnItemClick(binding.getProfile());
    }
}
