package com.khaledismail.popularpeoplemovies.view.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.khaledismail.popularpeoplemovies.R;
import com.khaledismail.popularpeoplemovies.listeners.PersonItemClickListener;
import com.khaledismail.popularpeoplemovies.model.database.data.PopularPerson;
import com.khaledismail.popularpeoplemovies.utils.Constants;
import com.squareup.picasso.Picasso;

public class PersonViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView nameTextView;
    private TextView departmentTextView;
    private ImageView profileImageView;
    private PopularPerson popularPerson;
    private PersonItemClickListener personItemClickListener;

    public PersonViewHolder(View view, PersonItemClickListener personItemClickListener) {
        super(view);
        this.nameTextView = view.findViewById(R.id.textView_name);
        this.departmentTextView = view.findViewById(R.id.textView_department);
        this.profileImageView = view.findViewById(R.id.imageView_profile);
        this.personItemClickListener = personItemClickListener;
        view.setOnClickListener(this);
    }

    public void bindPopularPerson(PopularPerson popularPerson) {
        this.popularPerson = popularPerson;
        nameTextView.setText(popularPerson.getName());
        departmentTextView.setText(popularPerson.getDepartment());
        Picasso.get().load(Constants.PERSON_IMAGE_BASE_URL + popularPerson.getProfilePath())
                .placeholder(R.drawable.ic_loading)
                .error(R.drawable.ic_person_placeholder).into(profileImageView);
    }

    @Override
    public void onClick(View view) {
        if (personItemClickListener != null) {
            personItemClickListener.OnItemClick(popularPerson);
        }
    }
}
