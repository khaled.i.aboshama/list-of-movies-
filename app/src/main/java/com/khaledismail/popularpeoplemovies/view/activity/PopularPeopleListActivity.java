package com.khaledismail.popularpeoplemovies.view.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.khaledismail.popularpeoplemovies.R;
import com.khaledismail.popularpeoplemovies.listeners.PersonItemClickListener;
import com.khaledismail.popularpeoplemovies.model.database.data.PopularPerson;
import com.khaledismail.popularpeoplemovies.utils.Constants;
import com.khaledismail.popularpeoplemovies.view.adapter.PopularPeopleAdapter;
import com.khaledismail.popularpeoplemovies.viewmodel.PopularPeopleViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PopularPeopleListActivity extends AppCompatActivity implements PersonItemClickListener {

    protected PopularPeopleViewModel viewModel;
    @BindView(R.id.recyclerView_popular_people)
    RecyclerView popularPeopleRecyclerView;
    @BindView(R.id.editText_search_popular_person)
    EditText popularPeopleEditText;
    @BindView(R.id.textView_no_result_found)
    TextView resultNoFoundTextView;
    @BindView(R.id.frameLayout_progressBar_overlay)
    FrameLayout progressBarOverlayFrameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popular_people_list);
        setupToolbar();
        ButterKnife.bind(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        popularPeopleRecyclerView.setLayoutManager(linearLayoutManager);
        viewModel = ViewModelProviders.of(this).get(PopularPeopleViewModel.class);
        setObserversOfViewModel();
        setListener();
    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
    }

    private void setListener() {
        popularPeopleEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                viewModel.updateQuery(editable.toString().trim());
            }
        });
    }


    private void setObserversOfViewModel() {
        PopularPeopleAdapter popularPeopleAdapter =
                new PopularPeopleAdapter(this);
        popularPeopleRecyclerView.setAdapter(popularPeopleAdapter);
        viewModel.getPopularPersonList().observe(this,
                popularPeopleAdapter::submitList);
        viewModel.showNoResultFound().observe(this, this::handleShowNoResultFound);
        viewModel.hideProgressbarOverlay().observe(this,
                this::handleHideProgressbarOverlay);
    }

    private void handleShowNoResultFound(Boolean showNoResult) {
        if (showNoResult)
            resultNoFoundTextView.setVisibility(View.VISIBLE);
        else
            resultNoFoundTextView.setVisibility(View.GONE);
    }

    private void handleHideProgressbarOverlay(Boolean hide) {
        if (hide)
            progressBarOverlayFrameLayout.setVisibility(View.GONE);
        else
            progressBarOverlayFrameLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void OnItemClick(PopularPerson popularPerson) {
        openPersonDetailsScreen(popularPerson);
    }

    private void openPersonDetailsScreen(PopularPerson popularPerson) {
        Intent intent = new Intent(this, PersonDetailsActivity.class);
        intent.putExtra(Constants.Intent.PERSON_KEY, popularPerson);
        startActivity(intent);
    }

    @Override
    public void onReachLastItem() {
        viewModel.onReachLastItem();
    }

    @Override
    protected void onDestroy() {
        viewModel.onDestroy();
        super.onDestroy();
    }
}
