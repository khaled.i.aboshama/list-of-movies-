package com.khaledismail.popularpeoplemovies.view.adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.ListAdapter;

import com.khaledismail.popularpeoplemovies.R;
import com.khaledismail.popularpeoplemovies.databinding.ListItemImageProfileBinding;
import com.khaledismail.popularpeoplemovies.listeners.ProfileImageClickListener;
import com.khaledismail.popularpeoplemovies.model.response.Profile;
import com.khaledismail.popularpeoplemovies.view.viewholder.ImageViewHolder;

public class PersonImagesAdapter extends ListAdapter<Profile, ImageViewHolder> {
    private ProfileImageClickListener profileImageClickListener;

    public PersonImagesAdapter(ProfileImageClickListener profileImageClickListener) {
        super(Profile.DIFF_CALLBACK);
        this.profileImageClickListener = profileImageClickListener;
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ListItemImageProfileBinding binding =
                DataBindingUtil.inflate(
                        layoutInflater, R.layout.list_item_image_profile, parent, false);
        return new ImageViewHolder(binding, profileImageClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
        holder.getBinding().setProfile(getItem(position));
    }
}
