package com.khaledismail.popularpeoplemovies.view.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import com.khaledismail.popularpeoplemovies.R;
import com.khaledismail.popularpeoplemovies.databinding.ActivityImagePreviewBinding;
import com.khaledismail.popularpeoplemovies.utils.Constants;
import com.khaledismail.popularpeoplemovies.viewmodel.ImagePreviewViewModel;

public class ImagePreviewActivity extends AppCompatActivity {

    private static final int WRITE_STORAGE_REQUEST_CODE = 1001;
    private ActivityImagePreviewBinding binding;
    private ImagePreviewViewModel viewModel;
    private String personName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_image_preview);
        setupToolbar();
        parseIntentExtraData();
        viewModel = ViewModelProviders.of(this).get(ImagePreviewViewModel.class);

    }

    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.setTitle(getString(R.string.image_preview));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    private void parseIntentExtraData() {
        if (!getIntent().hasExtra(Constants.Intent.PROFILE_IMAGE_PATH_KEY))
            return;
        String profilePath = getIntent().getStringExtra(Constants.Intent.PROFILE_IMAGE_PATH_KEY);
        binding.setProfilePath(profilePath);
        if (!getIntent().hasExtra(Constants.Intent.PERSON_NAME_KEY))
            return;
        personName = getIntent().getStringExtra(Constants.Intent.PERSON_NAME_KEY);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.image_preview_menu, menu);
        return true;
    }

    private void saveImageInLocalStorage() {
        viewModel.saveImageInExternalStorage(
                (BitmapDrawable) binding.imageViewOriginalImage.getDrawable(), personName);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_image:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE) ==
                            PackageManager.PERMISSION_GRANTED)
                        saveImageInLocalStorage();
                    else
                        requestPermission();
                } else
                    saveImageInLocalStorage();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void requestPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE))
            Toast.makeText(this, getString(R.string.permission_description),
                    Toast.LENGTH_LONG).show();
        else
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    WRITE_STORAGE_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case WRITE_STORAGE_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    saveImageInLocalStorage();
                }
                break;
        }
    }
}
