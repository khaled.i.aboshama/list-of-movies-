package com.khaledismail.popularpeoplemovies.view.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ListAdapter;

import com.khaledismail.popularpeoplemovies.R;
import com.khaledismail.popularpeoplemovies.listeners.PersonItemClickListener;
import com.khaledismail.popularpeoplemovies.model.database.data.PopularPerson;
import com.khaledismail.popularpeoplemovies.view.viewholder.PersonViewHolder;

public class PopularPeopleAdapter extends ListAdapter<PopularPerson, PersonViewHolder> {

    private PersonItemClickListener personItemClickListener;

    public PopularPeopleAdapter(PersonItemClickListener personItemClickListener) {
        super(PopularPerson.DIFF_CALLBACK);
        this.personItemClickListener = personItemClickListener;
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.list_item_popular_person;
    }

    @NonNull
    @Override
    public PersonViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.list_item_popular_person, parent, false);
        return new PersonViewHolder(view, personItemClickListener);

    }

    @Override
    public void onBindViewHolder(@NonNull PersonViewHolder holder, int position) {
        holder.bindPopularPerson(getItem(position));
        if (position == getItemCount() - 1)
            personItemClickListener.onReachLastItem();
    }
}
