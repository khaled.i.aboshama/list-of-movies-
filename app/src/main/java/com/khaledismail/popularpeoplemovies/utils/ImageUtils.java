package com.khaledismail.popularpeoplemovies.utils;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.khaledismail.popularpeoplemovies.R;
import com.squareup.picasso.Picasso;

public class ImageUtils {


    @BindingAdapter({"bind:profileOriginalPath"})
    public static void loadOriginalImage(ImageView view, String imageUrl) {
        Picasso.get().load(Constants.PERSON_ORIGINAL_IMAGE_URL + imageUrl)
                .placeholder(R.drawable.ic_loading)
                .error(R.drawable.ic_person_placeholder).into(view);
    }

    @BindingAdapter({"bind:profileNormalPath"})
    public static void loadNormalImage(ImageView view, String imageUrl) {
        Picasso.get().load(Constants.PERSON_IMAGE_BASE_URL + imageUrl)
                .placeholder(R.drawable.ic_loading)
                .error(R.drawable.ic_person_placeholder).into(view);
    }

}
