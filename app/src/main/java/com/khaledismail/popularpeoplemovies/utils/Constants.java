package com.khaledismail.popularpeoplemovies.utils;

public class Constants {
    public static final String PAGE_REQUEST_PARAM = "page";
    public static final String QUERY_REQUEST_PARAM = "query";
    public static final String API_BASE_URL = "https://api.themoviedb.org/3/";
    public static final String PERSON_IMAGE_BASE_URL = "https://image.tmdb.org/t/p/w200";
    public static final String PERSON_ORIGINAL_IMAGE_URL = "https://image.tmdb.org/t/p/original";

    public static final String DATABASE_NAME = "popular people list database";
    public static final String POPULAR_PEOPLE_SHARED_PREFERENCE = "popular_people_shared_preference";
    public static final String LAST_PAGE_NUMBER_KEY = "last_page_number";
    public static final String TOTAL_PAGE_NUMBER_KEY = "total_page_number_key";
    public static final String SEARCH_TOTAL_PAGE_NUMBER_KEY = "search_total_page_number_key";
    public static final String SEARCH_LAST_PAGE_NUMBER_KEY = "search_last_page_number_key";
    public static final int ZERO_NUMBER = 0;
    public static final String API_MOVIES_KEY = "2da587d26f5cb204bd916fa9ab416582";
    public static final int DEFAULT_TOTAL_PAGE_NUMBER = -1;

    public static class Intent {
        public static final String PERSON_KEY = "person_key";
        public static final String PROFILE_IMAGE_PATH_KEY = "profile_image_path_key";
        public static final String PERSON_NAME_KEY = "person_name_key";
    }
}
