package com.khaledismail.popularpeoplemovies.repository;

import android.content.Context;

import com.khaledismail.popularpeoplemovies.model.response.PersonDetailsResponse;
import com.khaledismail.popularpeoplemovies.model.response.PopularPersonImagesResponse;

import io.reactivex.Single;

public interface PersonDetailsRepositoryContract {
    Single<PersonDetailsResponse> getPersonDetailsRequest(int personId, Context context);

    Single<PopularPersonImagesResponse> getPersonProfileImagesRequest(int personId,
                                                                      Context context);
}
