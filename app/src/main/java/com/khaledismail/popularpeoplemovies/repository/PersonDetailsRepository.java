package com.khaledismail.popularpeoplemovies.repository;

import android.content.Context;

import com.khaledismail.popularpeoplemovies.model.network.NetworkManager;
import com.khaledismail.popularpeoplemovies.model.network.api.PopularPeopleApi;
import com.khaledismail.popularpeoplemovies.model.response.PersonDetailsResponse;
import com.khaledismail.popularpeoplemovies.model.response.PopularPersonImagesResponse;
import com.khaledismail.popularpeoplemovies.utils.Constants;

import io.reactivex.Single;

public class PersonDetailsRepository implements PersonDetailsRepositoryContract {
    private static PersonDetailsRepository instance;

    private PersonDetailsRepository() {
    }

    public static PersonDetailsRepository getInstance() {
        if (instance == null) {
            instance = new PersonDetailsRepository();
        }
        return instance;
    }


    @Override
    public Single<PersonDetailsResponse> getPersonDetailsRequest(int personId, Context context) {
        PopularPeopleApi popularPeopleApi =
                NetworkManager.getInstance(context).providePopularPeopleApi();
        return popularPeopleApi.getPersonDetails(personId, Constants.API_MOVIES_KEY);
    }

    @Override
    public Single<PopularPersonImagesResponse> getPersonProfileImagesRequest(int personId,
                                                                             Context context) {
        PopularPeopleApi popularPeopleApi =
                NetworkManager.getInstance(context).providePopularPeopleApi();
        return popularPeopleApi.getPopularPersonImages(personId, Constants.API_MOVIES_KEY);
    }
}
