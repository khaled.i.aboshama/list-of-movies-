package com.khaledismail.popularpeoplemovies.repository;

import android.content.Context;

import androidx.lifecycle.LiveData;

import com.khaledismail.popularpeoplemovies.model.database.data.PopularPerson;
import com.khaledismail.popularpeoplemovies.model.response.PopularPeopleResponse;

import java.util.List;

import io.reactivex.Observable;

public interface PopularPeopleRepositoryContract {
    LiveData<List<PopularPerson>> selectCachedPopularPersonList(Context context, String query);

    Observable<PopularPeopleResponse> getPopularPeopleRequest(Context context);

    Observable<Boolean> storePopularPersonListInLocalDatabase(Context context,
                                                              List<PopularPerson> PopularPersonList);

    Observable<PopularPeopleResponse> searchPopularPeopleRequest(Context context, String query);

    boolean isCachedDataEmpty();

    void updatePageNumber(int pageNumber);

    void updateTotalPages(int totalPages);

    int getIntFromSharedPreference(String key);

    boolean isAllPopularPeopleDisplayed();

    boolean isAllSearchPopularPeopleDisplayed();

    void updateSearchPageNumber(int pageNumber);

    void updateSearchTotalPages(int pageNumber);
}
