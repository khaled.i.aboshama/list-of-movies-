package com.khaledismail.popularpeoplemovies.repository;

import android.content.Context;

import androidx.lifecycle.LiveData;

import com.khaledismail.popularpeoplemovies.model.database.PopularPeopleDatabase;
import com.khaledismail.popularpeoplemovies.model.database.PopularPeopleDatabaseContract;
import com.khaledismail.popularpeoplemovies.model.database.data.PopularPerson;
import com.khaledismail.popularpeoplemovies.model.network.NetworkManager;
import com.khaledismail.popularpeoplemovies.model.network.api.PopularPeopleApi;
import com.khaledismail.popularpeoplemovies.model.response.PopularPeopleResponse;
import com.khaledismail.popularpeoplemovies.model.shared_pref.PopularPeopleSharedPreference;
import com.khaledismail.popularpeoplemovies.model.shared_pref.SharedPreferenceContract;
import com.khaledismail.popularpeoplemovies.utils.Constants;

import java.util.List;

import io.reactivex.Observable;

public class PopularPeopleRepository implements PopularPeopleRepositoryContract {
    private static PopularPeopleRepository instance;
    private SharedPreferenceContract sharedPreferenceContract;
    private PopularPeopleDatabaseContract peopleDatabaseContract;

    private PopularPeopleRepository(Context context) {
        sharedPreferenceContract = PopularPeopleSharedPreference.getInstance(context);
        peopleDatabaseContract = PopularPeopleDatabase.getInstance(context);
    }

    public static PopularPeopleRepository getInstance(Context context) {
        if (instance == null) {
            instance = new PopularPeopleRepository(context);
        }
        return instance;
    }

    @Override
    public LiveData<List<PopularPerson>> selectCachedPopularPersonList(Context context,
                                                                       String query) {
        return peopleDatabaseContract.getPopularPeople("%" + query + "%");
    }

    @Override
    public Observable<PopularPeopleResponse> getPopularPeopleRequest(Context context) {
        PopularPeopleApi popularPeopleApi =
                NetworkManager.getInstance(context).providePopularPeopleApi();
        int pageNumber = sharedPreferenceContract.getInt(Constants.LAST_PAGE_NUMBER_KEY) + 1;
        return popularPeopleApi.getPopularPeople(pageNumber, Constants.API_MOVIES_KEY);
    }

    @Override
    public Observable<Boolean> storePopularPersonListInLocalDatabase(
            Context context,
            List<PopularPerson> PopularPersonList) {
        for (PopularPerson PopularPerson : PopularPersonList) {
            peopleDatabaseContract.getPopularPeopleDao().insertPopularPerson(PopularPerson);
        }
        return Observable.just(true);
    }

    @Override
    public boolean isCachedDataEmpty() {
        return sharedPreferenceContract.getInt(Constants.LAST_PAGE_NUMBER_KEY)
                == Constants.ZERO_NUMBER;
    }

    @Override
    public void updatePageNumber(int pageNumber) {
        sharedPreferenceContract.saveNumber(Constants.LAST_PAGE_NUMBER_KEY, pageNumber);
    }

    @Override
    public void updateTotalPages(int totalPages) {
        sharedPreferenceContract.saveNumber(Constants.TOTAL_PAGE_NUMBER_KEY, totalPages);
    }

    @Override
    public int getIntFromSharedPreference(String key) {
        return sharedPreferenceContract.getInt(key);
    }

    @Override
    public boolean isAllPopularPeopleDisplayed() {
        int pageNumber = sharedPreferenceContract.getInt(Constants.LAST_PAGE_NUMBER_KEY);
        int totalPageNumber =
                sharedPreferenceContract.getInt(Constants.TOTAL_PAGE_NUMBER_KEY);
        return (pageNumber == totalPageNumber
                && totalPageNumber != Constants.DEFAULT_TOTAL_PAGE_NUMBER);
    }

    @Override
    public boolean isAllSearchPopularPeopleDisplayed() {
        int pageNumber = sharedPreferenceContract.getInt(Constants.SEARCH_LAST_PAGE_NUMBER_KEY);
        int totalPageNumber =
                sharedPreferenceContract.getInt(Constants.SEARCH_TOTAL_PAGE_NUMBER_KEY);
        return (pageNumber == totalPageNumber
                && totalPageNumber != Constants.DEFAULT_TOTAL_PAGE_NUMBER);
    }

    @Override
    public void updateSearchPageNumber(int pageNumber) {
        sharedPreferenceContract.saveNumber(Constants.SEARCH_LAST_PAGE_NUMBER_KEY, pageNumber);
    }

    @Override
    public void updateSearchTotalPages(int pageNumber) {
        sharedPreferenceContract.saveNumber(Constants.SEARCH_TOTAL_PAGE_NUMBER_KEY, pageNumber);
    }

    @Override
    public Observable<PopularPeopleResponse> searchPopularPeopleRequest(Context context,
                                                                        String query) {
        PopularPeopleApi popularPeopleApi =
                NetworkManager.getInstance(context).providePopularPeopleApi();
        int pageNumber = sharedPreferenceContract.getInt(Constants.SEARCH_LAST_PAGE_NUMBER_KEY) + 1;
        return popularPeopleApi.searchPopularPeople(pageNumber, query, Constants.API_MOVIES_KEY);
    }
}
