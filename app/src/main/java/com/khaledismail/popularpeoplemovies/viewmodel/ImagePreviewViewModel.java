package com.khaledismail.popularpeoplemovies.viewmodel;

import android.app.Application;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Environment;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.khaledismail.popularpeoplemovies.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImagePreviewViewModel extends AndroidViewModel {

    public ImagePreviewViewModel(@NonNull Application application) {
        super(application);
    }

    public void saveImageInExternalStorage(BitmapDrawable drawable, String personName) {
        Bitmap bitmap = drawable.getBitmap();
        FileOutputStream outStream;
        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File(sdCard.getAbsolutePath() + "/popular_images");
        dir.mkdirs();
        String fileName = String.format(personName + "_%d.jpg", System.currentTimeMillis());
        File outFile = new File(dir, fileName);
        try {
            outStream = new FileOutputStream(outFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
            outStream.flush();
            outStream.close();
            Toast.makeText(
                    getApplication().getApplicationContext(),
                    getApplication().getString(R.string.image_saved_successfully),
                    Toast.LENGTH_LONG).show();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
