package com.khaledismail.popularpeoplemovies.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import com.khaledismail.popularpeoplemovies.model.database.data.PopularPerson;
import com.khaledismail.popularpeoplemovies.repository.PopularPeopleRepository;
import com.khaledismail.popularpeoplemovies.repository.PopularPeopleRepositoryContract;
import com.khaledismail.popularpeoplemovies.utils.Constants;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class PopularPeopleViewModel extends AndroidViewModel {
    private LiveData<List<PopularPerson>> popularPersonLiveData;
    private LiveData<Boolean> showNoResultFoundView;
    private LiveData<Boolean> hideProgressBarView;
    private MutableLiveData<String> queryLiveData = new MutableLiveData<>();
    private MutableLiveData<Boolean> finishedRequest = new MutableLiveData<>();
    private CompositeDisposable compositeDisposable;
    private PopularPeopleRepositoryContract repository;
    private boolean isSearchEnabled;

    public PopularPeopleViewModel(@NonNull Application application) {
        super(application);
        repository = PopularPeopleRepository.getInstance(getApplication().getApplicationContext());
        if (compositeDisposable == null)
            compositeDisposable = new CompositeDisposable();
        queryLiveData.setValue("");
        finishedRequest.setValue(true);
        updateLocalCachedData();
        handleDisplayPopularPeopleList();
        setupPopularPeopleLiveData();
        setupShowNoResultFoundLiveData();
        setupShowProgressbarOverlayLiveData();
    }

    private void updateLocalCachedData() {
        if (repository.getIntFromSharedPreference(Constants.TOTAL_PAGE_NUMBER_KEY)
                == Constants.ZERO_NUMBER)
            repository.updateTotalPages(Constants.DEFAULT_TOTAL_PAGE_NUMBER);
        if (repository.getIntFromSharedPreference(Constants.SEARCH_TOTAL_PAGE_NUMBER_KEY)
                == Constants.ZERO_NUMBER)
            repository.updateSearchTotalPages(Constants.DEFAULT_TOTAL_PAGE_NUMBER);
    }

    private void setupShowProgressbarOverlayLiveData() {
        hideProgressBarView = Transformations.switchMap(finishedRequest,
                finishedRequest -> {
                    MutableLiveData<Boolean> result = new MutableLiveData<>();
                    if (finishedRequest)
                        result.setValue(true);
                    else
                        result.setValue(false);
                    return result;
                });
    }

    private void setupShowNoResultFoundLiveData() {
        showNoResultFoundView = Transformations.switchMap(popularPersonLiveData,
                popularList -> {
                    MutableLiveData<Boolean> result = new MutableLiveData<>();
                    if (popularList == null || popularList.isEmpty())
                        result.setValue(true);
                    else
                        result.setValue(false);
                    return result;
                });
    }

    private void setupPopularPeopleLiveData() {
        popularPersonLiveData = Transformations.switchMap(queryLiveData,
                query -> {
                    repository.updateSearchPageNumber(0);
                    repository.updateSearchTotalPages(Constants.DEFAULT_TOTAL_PAGE_NUMBER);
                    LiveData<List<PopularPerson>> result =
                            repository.selectCachedPopularPersonList(
                                    getApplication().getApplicationContext(), query);
                    if (!query.isEmpty())
                        handleSendSearchQueryRequest();
                    return result;
                });
    }

    private void handleSendSearchQueryRequest() {
        String query = queryLiveData.getValue();
        if (query == null || query.isEmpty())
            return;
        finishedRequest.setValue(false);
        compositeDisposable.add(
                repository.searchPopularPeopleRequest(
                        getApplication().getApplicationContext(), query)
                        .doOnNext(response -> {
                            repository.storePopularPersonListInLocalDatabase(
                                    getApplication().getApplicationContext(),
                                    response.getPopularPersonArrayList());
                            repository.updateSearchPageNumber(response.getPageNumber());
                            repository.updateSearchTotalPages(response.getTotalPages());
                        })
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                response -> finishedRequest.setValue(true),
                                error -> finishedRequest.setValue(true)
                        ));

    }

    private void handleDisplayPopularPeopleList() {
        if (repository.isCachedDataEmpty())
            sendPopularPeopleRequest();
    }

    private void handleNextPagePopularPeopleRequest() {
        if (isSearchEnabled) {
            if (!repository.isAllSearchPopularPeopleDisplayed())
                handleSendSearchQueryRequest();
        } else {
            if (finishedRequest.getValue() != null &&
                    (!finishedRequest.getValue() || repository.isAllPopularPeopleDisplayed()))
                return;
            sendPopularPeopleRequest();
        }
    }

    private void sendPopularPeopleRequest() {
        finishedRequest.setValue(false);
        compositeDisposable.add(
                repository.getPopularPeopleRequest(getApplication().getApplicationContext())
                        .subscribeOn(Schedulers.io())
                        .doOnNext(response -> {
                            repository.storePopularPersonListInLocalDatabase(
                                    getApplication().getApplicationContext(),
                                    response.getPopularPersonArrayList());
                            repository.updatePageNumber(response.getPageNumber());
                            repository.updateTotalPages(response.getTotalPages());
                        })
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(response -> finishedRequest.setValue(true),
                                error -> finishedRequest.setValue(true)));
    }

    public LiveData<List<PopularPerson>> getPopularPersonList() {
        return popularPersonLiveData;
    }

    public void onReachLastItem() {
        handleNextPagePopularPeopleRequest();
    }

    public void updateQuery(String personName) {
        if (personName.isEmpty())
            isSearchEnabled = false;
        else
            isSearchEnabled = true;
        if (queryLiveData.getValue() != null &&
                queryLiveData.getValue().equalsIgnoreCase(personName)) {
            return;
        }
        queryLiveData.setValue(personName);
    }

    public LiveData<Boolean> showNoResultFound() {
        return showNoResultFoundView;
    }

    public LiveData<Boolean> hideProgressbarOverlay() {
        return hideProgressBarView;
    }

    public void onDestroy() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed())
            compositeDisposable.dispose();
    }
}
