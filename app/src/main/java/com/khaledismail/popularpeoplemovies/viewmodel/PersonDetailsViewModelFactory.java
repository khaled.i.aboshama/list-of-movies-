package com.khaledismail.popularpeoplemovies.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class PersonDetailsViewModelFactory implements ViewModelProvider.Factory {
    private Application mApplication;
    private int personId;

    public PersonDetailsViewModelFactory(Application application, int personId) {
        mApplication = application;
        this.personId = personId;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new PersonDetailsViewModel(mApplication, personId);
    }
}
