package com.khaledismail.popularpeoplemovies.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.khaledismail.popularpeoplemovies.model.response.PersonDetailsResponse;
import com.khaledismail.popularpeoplemovies.model.response.Profile;
import com.khaledismail.popularpeoplemovies.repository.PersonDetailsRepository;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class PersonDetailsViewModel extends AndroidViewModel {
    private PersonDetailsRepository repository;
    private CompositeDisposable compositeDisposable;
    private MutableLiveData<List<Profile>> profileLiveData;
    private MutableLiveData<PersonDetailsResponse> personDetailsResponseLiveData;
    private int personId;

    protected PersonDetailsViewModel(@NonNull Application application, int personId) {
        super(application);
        this.personId = personId;
        repository = PersonDetailsRepository.getInstance();
        personDetailsResponseLiveData = new MutableLiveData<>();
        profileLiveData = new MutableLiveData<>();
        if (compositeDisposable == null)
            compositeDisposable = new CompositeDisposable();
    }

    public void setupDataScreen() {
        sendPersonDetailsRequest();
        sendPersonProfileImagesRequest();
    }


    private void sendPersonProfileImagesRequest() {
        if (personId == 0)
            return;
        compositeDisposable.add(
                repository.getPersonProfileImagesRequest(
                        personId, getApplication().getApplicationContext())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                response -> profileLiveData.setValue(response.getProfileArrayList()),
                                this::handleErrorMessage));
    }

    private void sendPersonDetailsRequest() {
        if (personId == 0)
            return;
        compositeDisposable.add(
                repository.getPersonDetailsRequest(
                        personId, getApplication().getApplicationContext())
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(response -> personDetailsResponseLiveData.setValue(response),
                                this::handleErrorMessage));
    }

    private void handleErrorMessage(Throwable error) {

    }

    public LiveData<List<Profile>> getProfileLiveData() {
        return profileLiveData;
    }

    public MutableLiveData<PersonDetailsResponse> getPersonDetailsResponseLiveData() {
        return personDetailsResponseLiveData;
    }

    public void onDestroy() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed())
            compositeDisposable.dispose();
    }
}
