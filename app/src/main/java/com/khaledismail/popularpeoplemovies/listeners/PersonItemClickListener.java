package com.khaledismail.popularpeoplemovies.listeners;

import com.khaledismail.popularpeoplemovies.model.database.data.PopularPerson;

public interface PersonItemClickListener {
    void OnItemClick(PopularPerson popularPerson);

    void onReachLastItem();

}
