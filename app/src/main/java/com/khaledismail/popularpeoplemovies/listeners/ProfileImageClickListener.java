package com.khaledismail.popularpeoplemovies.listeners;

import com.khaledismail.popularpeoplemovies.model.response.Profile;

public interface ProfileImageClickListener {
    void OnItemClick(Profile profile);

}
